require "csv"
class TweetsController < ApplicationController
    def new
        @errors = []
    end
    
    # 新規処理のアクション
    def create
        @errors = []
        if params[:name].empty?
            @errors << 'ユーザ名が未入力です'
        end
        if params[:tweet].empty?
            @errors << 'つぶやき内容が未入力です'
        end
        if params[:tweet].length > 140
            @errors << 'つぶやきは140文字以内で入力してください'
        end
        
        # new は view の名前。and return は　以後の処理を終了するおまじない
        if @errors.present?
            render 'new' and return
        end
        
        @tweet = Tweet.new(name: params[:name], tweet: params[:tweet])
        if @tweet.save
            redirect_to(root_path)
        else
            render 'new' and return
        end
    end
end

