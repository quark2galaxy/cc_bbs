Rails.application.routes.draw do
  #get 'static_pages/top'
  root 'static_pages#top'
  #get '/tweets', to:'tweets#new'
  # link_to にしか使えない
  get '/tweets', to: 'tweets#new', as: 'new_tweet'
  post '/tweets/create', to: 'tweets#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
